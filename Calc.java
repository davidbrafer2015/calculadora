import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class Calc extends JFrame implements ActionListener {

		JTextField visor;
		JButton b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,bmenos,bmais,bdiv,bmult,bresult,bclear;
		String valor1 = "",valor2 = "";
		int cont = 1;
		String sinal ="";
		
		public Calc ()throws Exception {
			super("Calculadora");
			this.setSize(253,350);
			this.setLocationRelativeTo(null);
			this.setLayout(null);
			this.setVisible(true);
		
			visor = new JTextField();
			visor.setBounds(3,10,230,30);
			visor.setEnabled(true);
			this.add(visor);
			
			b1 = new JButton("1");
			b1.setBounds(3, 60, 50, 50);
			b1.addActionListener(this);
			this.add(b1);
			
			b2 = new JButton("2");
			b2.setBounds(63, 60, 50, 50);
			b2.addActionListener(this);
			this.add(b2);
			
			b3 = new JButton("3");
			b3.setBounds(123, 60, 50, 50);
			b3.addActionListener(this);
			this.add(b3);
			
			b4 = new JButton("4");
			b4.setBounds(3, 120, 50, 50);
			b4.addActionListener(this);
			this.add(b4);
			
			b5 = new JButton("5");
			b5.setBounds(63, 120, 50, 50);
			b5.addActionListener(this);
			this.add(b5);
			
			b6 = new JButton("6");
			b6.setBounds(123, 120, 50, 50);
			b6.addActionListener(this);
			this.add(b6);
			
			b7 = new JButton("7");
			b7.setBounds(3, 180, 50, 50);
			b7.addActionListener(this);
			this.add(b7);
			
			b8 = new JButton("8");
			b8.setBounds(63, 180, 50, 50);
			b8.addActionListener(this);
			this.add(b8);
			
			b9 = new JButton("9");
			b9.setBounds(123, 180, 50, 50);
			b9.addActionListener(this);
			this.add(b9);
			
			b0 = new JButton("0");
			b0.setBounds(3, 240, 50, 50);
			b0.addActionListener(this);
			this.add(b0);
			
			bmenos = new JButton("-");
			bmenos.setBounds(183, 180, 50, 50);
			bmenos.addActionListener(this);
			this.add(bmenos);
			
			bmais = new JButton("+");
			bmais.setBounds(183, 240, 50, 50);
			bmais.addActionListener(this);
			this.add(bmais);
			
			bdiv = new JButton("/");
			bdiv.setBounds(183, 60, 50, 50);
			bdiv.addActionListener(this);
			this.add(bdiv);
			
			bmult = new JButton("*");
			bmult.setBounds(183, 120, 50, 50);
			bmult.addActionListener(this);
			this.add(bmult);
			
			bresult = new JButton("=");
			bresult.setBounds(123, 240, 50, 50);
			bresult.addActionListener(this);
			this.add(bresult);
			
			bclear = new JButton("C");
			bclear.setBounds(63, 240, 50, 50);
			bclear.addActionListener(this);
			this.add(bclear);
			
			this.setEnabled(true);
			
		}
		
		public static void main(String[] args) throws Exception{
			new Calc();
		
		}
		

			
		
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == b1) {
				
				String n = "1";
				
				if(cont == 1) {
					
					valor1 = valor1 + n;
					visor.setText(valor1);
					
				}else if(cont == 2){
					valor2 = valor2 + n;
					visor.setText(valor2);
				}
				
				
			}else if(e.getSource() == b2) {

				String n = "2";
				
				if(cont == 1) {
					
					valor1 = valor1 + n;
					visor.setText(valor1);
					
				}else if(cont == 2){
					valor2 = valor2 + n;
					visor.setText(valor2);
				}
				
			}else if(e.getSource() == b3) {

				String n = "3";
				
				if(cont == 1) {
					
					valor1 = valor1 + n;
					visor.setText(valor1);
					
				}else if(cont == 2){
					valor2 = valor2 + n;
					visor.setText(valor2);
				}
				
			}else if(e.getSource() == b4) {

				String n = "4";
				
				if(cont == 1) {
					
					valor1 = valor1 + n;
					visor.setText(valor1);
					
				}else if(cont == 2){
					valor2 = valor2 + n;
					visor.setText(valor2);
				}
			}else if(e.getSource() == b5) {

				String n = "5";
				
				if(cont == 1) {
					
					valor1 = valor1 + n;
					visor.setText(valor1);
					
				}else if(cont == 2){
					valor2 = valor2 + n;
					visor.setText(valor2);
				}
			}else if(e.getSource() == b6) {

				String n = "6";
				
				if(cont == 1) {
					
					valor1 = valor1 + n;
					visor.setText(valor1);
					
				}else if(cont == 2){
					valor2 = valor2 + n;
					visor.setText(valor2);
				}
			}else if(e.getSource() == b7) {

				String n = "7";
				
				if(cont == 1) {
					
					valor1 = valor1 + n;
					visor.setText(valor1);
					
				}else if(cont == 2){
					valor2 = valor2 + n;
					visor.setText(valor2);
				}
			}else if(e.getSource() == b8) {

				String n = "8";
				
				if(cont == 1) {
					
					valor1 = valor1 + n;
					visor.setText(valor1);
					
				}else if(cont == 2){
					valor2 = valor2 + n;
					visor.setText(valor2);
				}
			}else if(e.getSource() == b9) {

				String n = "9";
				
				if(cont == 1) {
					
					valor1 = valor1 + n;
					visor.setText(valor1);
					
				}else if(cont == 2){
					valor2 = valor2 + n;
					visor.setText(valor2);
				}
			}else if(e.getSource() == b0) {

				String n = "0";
				
				if(cont == 1) {
					
					valor1 = valor1 + n;
					visor.setText(valor1);
					
				}else if(cont == 2){
					valor2 = valor2 + n;
					visor.setText(valor2);
				}
			}else if(e.getSource() == bmenos) {
				
				sinal = "menos";
				cont = 2;
				visor.setText(" ");
				
			}else if(e.getSource() == bmais) {
				
				sinal = "mais";
				cont = 2;
				visor.setText(" ");
				
			}else if(e.getSource() == bdiv) {
				
				sinal = "dividir";
				cont = 2;
				visor.setText(" ");
								
			}else if(e.getSource() == bmult) {
				
				sinal = "multiplicar";
				cont = 2;
				visor.setText(" ");
				
			}else if(e.getSource() == bresult) {
				
				if(sinal =="mais") {
					
					int num1 = Integer.parseInt(valor1);
					int num2 = Integer.parseInt(valor2);
					int resultado = num1 + num2;
					
					visor.setText(""+resultado);
					valor1 = "";
					valor2 = "";
					
				}else if(sinal == "menos") {

					int num1 = Integer.parseInt(valor1);
					int num2 = Integer.parseInt(valor2);
					int resultado = num1 - num2;
					
					visor.setText(""+resultado);
					valor1 = "";
					valor2 = "";
					
				}else if(sinal == "dividir") {

					double num1 = Integer.parseInt(valor1);
					double num2 = Integer.parseInt(valor2);
					double resultado = num1 / num2;
					
					visor.setText(""+resultado);
					valor1 = "";
					valor2 = "";
					
				}else if(sinal == "multiplicar") {

					double num1 = Integer.parseInt(valor1);
					double num2 = Integer.parseInt(valor2);
					double resultado = num1 * num2;
					
					visor.setText(""+resultado);
					valor1 = "";
					valor2 = "";
				}
				
			}else if(e.getSource() == bclear) {
				
				visor.setText(" ");
				valor1 = "";
				valor2 = "";
				
				
			}
								
	}
}
